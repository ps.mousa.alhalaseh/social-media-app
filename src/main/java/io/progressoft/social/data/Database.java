package io.progressoft.social.data;

import io.progressoft.social.entities.User;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Database {
    private List<User> users = new ArrayList<>();

    public Database() {
        User mousa = new User(1, "Mousa Halaseh", 23);
        User ahmad = new User(2, "Ahmad Asfour", 18);
        this.save(mousa);
        this.save(ahmad);
    }

    public List<User> getAllUsers() {
        return users;
    }

    public User save(User user) {
        this.users.add(user);
        return user;
    }

    public void delete(int id) {
        users.removeIf(user -> user.getId() == id);
    }

    public User getById(int id) {
        return users.stream()
                .filter(user -> user.getId() == id)
                .findAny()
                .orElse(null);
    }
}
